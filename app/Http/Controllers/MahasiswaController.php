<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('mahasiswa', [
            'mahasiswa' => Mahasiswa::all()
        ]);
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        //validate form
        $this->validate($request, [
            'nama'     => 'required|min:5',
            'nbi'   => 'required|min:5'
        ]);

        //create post
        Mahasiswa::create([
            'nama'     => $request->nama,
            'nbi'   => $request->nbi
        ]);

        //redirect to index
        return redirect()->route('index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    public function show(string $id)
    {
        //get post by ID
        $mahasiswa = Mahasiswa::findOrFail($id);

        //render view with mahasiswa
        return view('show', compact('mahasiswa'));
    }

    public function update(Request $request, $id)
    {
        //validate form
        $this->validate($request, [
            'nama'     => 'required|min:5',
            'nbi'   => 'required|min:5'
        ]);

        $mahasiswa = Mahasiswa::findOrFail($id);

        $mahasiswa->update([
            'nama'     => $request->nama,
            'nbi'   => $request->nbi
        ]);

        //redirect to index
        return redirect()->route('index')->with(['success' => 'Data Berhasil Diubah!']);
    }

    public function del($id)
    {
        $mahasiswa = Mahasiswa::findOrFail($id);

        $mahasiswa->delete();

        return redirect()->route('index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}
