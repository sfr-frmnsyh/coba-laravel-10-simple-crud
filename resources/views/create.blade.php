@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('store') }}" method="POST">

            @csrf

            <div class="form-group">
                <label class="font-weight-bold">Nama</label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                    value="{{ old('nama') }}" placeholder="Masukkan Nama">
            </div>

            <div class="form-group">
                <label class="font-weight-bold">nbi</label>
                <input type="text" class="form-control @error('nbi') is-invalid @enderror" name="nbi"
                    value="{{ old('nbi') }}" placeholder="Masukkan nbi">
            </div>


            <div class="mt-3">
                <button type="submit" class="btn btn-md btn-success">SIMPAN</button>
                <button type="reset" class="btn btn-md btn-warning">RESET</button>
            </div>
        </form>
    </div>
@endsection
