@extends('layouts.app')

@section('content')
    <div class="container">

        <div>
            <a class="btn btn-success mb-3" href="{{ route('create') }}">Create Data</a>
        </div>

        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">NBI</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($mahasiswa as $mhs)
                        <tr>
                            <th scope="row">{{ $mhs->id }}</th>
                            <td>{{ $mhs->nama }}</td>
                            <td>{{ $mhs->nbi }}</td>
                            <td>
                                <a class="btn btn-warning" href="{{ route('show', $mhs->id) }}">Update</a>
                                <a class="btn btn-danger" href="{{ route('del', $mhs->id) }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
