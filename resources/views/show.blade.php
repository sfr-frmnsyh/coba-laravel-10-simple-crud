@extends('layouts.app')

@section('content')
    <div class="container">
        <div>
            <form action="{{ route('update', ['id' => $mahasiswa->id]) }}" method="POST">

                @csrf

                <div class="form-group">
                    <label class="font-weight-bold">Nama</label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                        value="{{ $mahasiswa->nama }}" placeholder="Masukkan Nama">
                </div>

                <div class="form-group">
                    <label class="font-weight-bold">NBI</label>
                    <input type="text" class="form-control @error('nbi') is-invalid @enderror" name="nbi"
                        value="{{ $mahasiswa->nbi }}" placeholder="Masukkan nbi">
                </div>

                <div class="mt-3">
                    <button type="submit" class="btn btn-md btn-primary">Simpan</button>
                    <a href="{{ route('index') }}" class="btn btn-md btn-secondary">Back</a>
                </div>
            </form>
        </div>
    </div>
@endsection
