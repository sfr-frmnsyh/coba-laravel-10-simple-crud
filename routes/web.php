<?php

use App\Http\Controllers\MahasiswaController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return 'hello world!';
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/mahasiswa', [MahasiswaController::class, 'index'])->name('index');
Route::get('/create', [MahasiswaController::class, 'create'])->name('create');
Route::post('/store', [MahasiswaController::class, 'store'])->name('store');
Route::get('/show/{id}', [MahasiswaController::class, 'show'])->name('show');
Route::post('/update/{id}', [MahasiswaController::class, 'update'])->name('update');
Route::get('/del/{id}', [MahasiswaController::class, 'del'])->name('del');
