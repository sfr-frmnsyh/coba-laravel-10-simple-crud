<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('12345678')
        ]);
        \App\Models\Mahasiswa::create([
            'nama' => 'Safri',
            'nbi' => '1461800144',
        ]);
        \App\Models\Mahasiswa::create([
            'nama' => 'Adit',
            'nbi' => '1461800166',
        ]);
        \App\Models\Mahasiswa::create([
            'nama' => 'Ferry',
            'nbi' => '1461800188',
        ]);
    }
}
